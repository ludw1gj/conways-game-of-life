export interface PainterOptions {
  ctx: CanvasRenderingContext2D;
  cellSize: number;
  padding: number;
}

export declare type Painter = (options: PainterOptions) => void;

/** A readonly 2D number array. */
export type Grid = ReadonlyArray<ReadonlyArray<number>>;

/** Dimensions of a 2D Grid. */
export interface GridDimensions {
  /** Number of rows. */
  numRows: number;
  /** Number of columns. */
  numCols: number;
}

/** Position of a 2D Grid. */
interface Position {
  /** The x position. */
  x: number;
  /** The y position. */
  y: number;
}

/** Conway's Game of Life. */
export class GameOfLife {
  private readonly grid: Grid;
  private readonly gridDimensions: GridDimensions;

  /** Create a conway's game of life game. */
  constructor(gridDimensions: GridDimensions, grid?: Grid) {
    this.gridDimensions = gridDimensions;
    this.grid = grid ? grid : GameOfLife.createRandomGrid(gridDimensions);
  }

  /** Conduct a step. */
  public step(): GameOfLife {
    return new GameOfLife(this.gridDimensions, this.generateNextGrid());
  }

  /** Generate a painter function that can paint the game grid. */
  public generatePainter(framesPerSecond: number): Painter {
    const stepper = (options: PainterOptions, game: GameOfLife): void => {
      const { ctx, cellSize, padding } = options;

      const sidePadding = padding / 2;
      game.grid.forEach((row, y) =>
        row.forEach((cell, x) => {
          ctx.beginPath();
          ctx.rect(x * cellSize + sidePadding, y * cellSize + sidePadding, cellSize, cellSize);
          ctx.fillStyle = cell ? "black" : "white";
          ctx.fill();
          ctx.stroke();
        })
      );

      const steppedGame = game.step();
      setTimeout(() => {
        requestAnimationFrame(() => stepper(options, steppedGame));
      }, 1000 / framesPerSecond);
    };

    return (options: PainterOptions): void => {
      stepper(options, this);
    };
  }

  /** Generate the next state of the grid. */
  private generateNextGrid(): Grid {
    return this.grid.map((row, y) =>
      row.map((cell, x) => {
        const neighbours = this.countNeighbours({ x, y });
        const isAlive = GameOfLife.isCellAlive(cell);

        if (isAlive && neighbours < 2) {
          return 0;
        }
        if (isAlive && (neighbours === 2 || neighbours === 3)) {
          return 1;
        }
        if (isAlive && neighbours > 3) {
          return 0;
        }
        if (!isAlive && neighbours === 3) {
          return 1;
        }
        return cell;
      })
    );
  }

  /** Count the amount of live cells surround the cell of the given position. */
  private countNeighbours(position: Position): number {
    let count = 0;
    for (let y = -1; y < 2; y++) {
      for (let x = -1; x < 2; x++) {
        if (x === 0 && y === 0) {
          continue;
        }
        const currentYPos = y + position.y;
        const currentXPos = x + position.x;
        if (
          currentXPos >= 0 &&
          currentYPos >= 0 &&
          currentXPos < this.gridDimensions.numCols &&
          currentYPos < this.gridDimensions.numRows
        ) {
          const cell = this.grid[currentYPos][currentXPos];
          if (GameOfLife.isCellAlive(cell)) {
            count++;
          }
        }
      }
    }
    return count;
  }

  /** Create a random Grid. */
  private static createRandomGrid(dimensions: GridDimensions): Grid {
    const arr = Array(dimensions.numRows)
      .fill(undefined)
      .map(() => Array(dimensions.numCols).fill(undefined));
    return arr.map(row => row.map(() => Math.floor(Math.random() * 2)));
  }

  /** Check if cell is alive. */
  private static isCellAlive(cell: number): boolean {
    return cell === 1;
  }
}
