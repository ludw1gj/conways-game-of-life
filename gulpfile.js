var gulp = require("gulp");
var del = require("del");

function clean(cb) {
  del(["lib/"]);
  cb();
}

exports.clean = clean;
